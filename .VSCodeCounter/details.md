# Details

Date : 2020-06-13 21:30:30

Directory /Users/razvang./Documents/licenta/backend/controllers

Total : 2 files,  489 codes, 0 comments, 72 blanks, all 561 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [controllers/pets-controller.js](/controllers/pets-controller.js) | JavaScript | 270 | 0 | 34 | 304 |
| [controllers/users-controller.js](/controllers/users-controller.js) | JavaScript | 219 | 0 | 38 | 257 |

[summary](results.md)