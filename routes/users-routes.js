const express = require("express");
const { check } = require("express-validator");

const usersController = require("../controllers/users-controller");
const fileUpload = require("../middleware/file-upload");

const router = express.Router();

router.get("/", usersController.getUsers);

router.get("/:uid", usersController.getUserByUserId);

router.post(
  "/signup",
  fileUpload.single("image"),
  [
    check("name").not().isEmpty(),
    check("email").normalizeEmail().isEmail(),
    check("password").isLength({ min: 6 }),
  ],
  usersController.signup
);

router.patch(
  "/:uid",
  [check("name").not().isEmpty(), check("email").normalizeEmail().isEmail()],
  usersController.updateUser
);

router.patch("/:uid/quiz", usersController.updateUserType);

router.post("/login", usersController.login);

module.exports = router;
