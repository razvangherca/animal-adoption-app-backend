const express = require("express");
const { check } = require("express-validator");
const fileUpload = require("../middleware/file-upload");
const checkAuth = require("../middleware/check-auth");

const petsControllers = require("../controllers/pets-controller");

const router = express.Router();

router.get("/", petsControllers.getPets);

router.get("/:pid", petsControllers.getPetById);

router.get("/user/:uid", petsControllers.getPetsByUserId);

router.use(checkAuth);

router.post(
  "/",
  fileUpload.single("image"),
  [
    check("name").not().isEmpty(),
    check("description").isLength({ min: 5 }),
    check("address").not().isEmpty(),
  ],
  petsControllers.createPet
);

router.post("/:pid/contactOwner", petsControllers.sendMail);

router.patch(
  "/:pid",
  [
    check("name").not().isEmpty(),
    check("description").isLength({ min: 5 }),
    check("address").not().isEmpty(),
  ],
  petsControllers.updatePet
);

router.delete("/:pid", petsControllers.deletePet);

module.exports = router;
