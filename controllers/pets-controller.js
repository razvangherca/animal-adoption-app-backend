const fs = require("fs");
const HttpError = require("../models/http-error");
const { validationResult } = require("express-validator");
const { v4: uuid } = require("uuid");
const nodemailer = require("nodemailer");
const mongoose = require("mongoose");
const getCoordsFromAddress = require("../util/location");
const Pet = require("../models/pet");
const User = require("../models/user");

const sendMail = async (req, res, next) => {
  const petId = req.params.pid;
  let userEmail;
  try {
    userEmail = await Pet.aggregate([
      {
        $match: { _id: mongoose.Types.ObjectId(petId) },
      },
      {
        $lookup: {
          from: "users",
          localField: "creator",
          foreignField: "_id",
          as: "owner",
        },
      },
      {
        $unwind: "$owner",
      },
      {
        $project: {
          email: "$owner.email",
        },
      },
    ]);
  } catch (err) {
    const error = new HttpError(
      "Fetching user failed, please try again later",
      500
    );
    return next(error);
  }

  nodemailer.createTestAccount((err, accoiunt) => {
    const htmlEmail = `
      <h3>Contact details</h3>
      <ul>
        <li>Name: ${req.body.name}</li>
        <li>Email: ${req.body.email}</li>
      </ul>
      <h3>Message:</h3>
      <p>${req.body.contactForm}</p>
    `;

    let transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "animaladoptionapp@gmail.com",
        pass: "animaladoption96",
      },
    });

    let mailOptions = {
      from: req.body.email,
      to: userEmail[0].email,
      replyTo: req.body.email,
      subject: "Animal Adoption contact",
      text: req.body.contactForm,
      html: htmlEmail,
    };

    transporter.sendMail(mailOptions, (err, info) => {
      if (err) {
        return next(err);
      }
      res.json("ok");
    });
  });
};

const getPets = async (req, res, next) => {
  let pets;
  try {
    pets = await Pet.find({});
  } catch (err) {
    const error = new HttpError(
      "Fetching pets failed, please try again later",
      500
    );
    return next(error);
  }
  res.json({ pets: pets.map((pet) => pet.toObject({ getters: true })) });
};

const getPetById = async (req, res, next) => {
  const petId = req.params.pid;
  let pet;

  try {
    pet = await Pet.findById(petId);
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not find a pet.",
      500
    );
    return next(error);
  }

  if (!pet) {
    const error = new HttpError(
      "Could not find a pet for the provided id.",
      404
    );
    return next(error);
  }
  res.json({ pet: pet.toObject({ getters: true }) });
};

const getPetsByUserId = async (req, res, next) => {
  const userId = req.params.uid;
  let pets;
  try {
    pets = await Pet.find({ creator: userId });
  } catch (err) {
    const error = new HttpError(
      "Fetching pets failed.Please try again later.",
      500
    );
    return next(error);
  }

  if (!pets || pets.length === 0) {
    return next(
      new HttpError("Could not find pets for the provided user id.", 404)
    );
  }
  res.json({ pets: pets.map((pets) => pets.toObject({ getters: true })) });
};

const createPet = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors);
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 422)
    );
  }
  const {
    name,
    description,
    breed,
    breedCareSuggestions,
    userType,
    address,
  } = req.body;
  let coordinates;
  try {
    coordinates = await getCoordsFromAddress(address);
  } catch (error) {
    return next(error);
  }

  const createdPet = new Pet({
    name,
    description,
    breed,
    breedCareSuggestions,
    userType,
    address,
    location: coordinates,
    image: req.file.path,
    creator: req.userData.userId,
  });

  let user;
  try {
    user = await User.findById(req.userData.userId);
  } catch (err) {
    const error = new HttpError("Creating pet failed, please try again.", 500);
    return next(error);
  }

  if (!user) {
    const error = new HttpError("Could not find user for provided id", 404);
    return next(error);
  }

  console.log(user);

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await createdPet.save({ session: sess });
    user.pets.push(createdPet);
    await user.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    const error = new HttpError("Creating pet failed, please try again", 500);
    return next(error);
  }

  res.status(201).json({ pet: createdPet });
};

const updatePet = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 422)
    );
  }

  const { name, description, address } = req.body;
  const petId = req.params.pid;

  let pet;
  try {
    pet = await Pet.findById(petId);
  } catch (err) {
    const error = new HttpError("You are not allowed to edit this pet", 401);
    return next(error);
  }

  if (pet.creator.toString() !== req.userData.userId) {
    const error = new HttpError(
      "Something went wrong, could not update pet",
      500
    );
    return next(error);
  }

  pet.name = name;
  pet.description = description;
  pet.address = address;

  try {
    await pet.save();
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not update pet",
      500
    );
    console.log(err);
    return next(error);
  }

  res.status(200).json({ pet: pet.toObject({ getters: true }) });
};

const deletePet = async (req, res, next) => {
  const petId = req.params.pid;

  let pet;
  try {
    pet = await Pet.findById(petId).populate("creator");
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not delete pet.",
      500
    );
    return next(error);
  }

  if (!pet) {
    const error = new HttpError("Could not find pet for this id.", 404);
    return next(error);
  }

  if (pet.creator.id !== req.userData.userId) {
    const error = new HttpError("You are not allowed to delete this pet.", 403);
    return next(error);
  }

  const imagePath = pet.image;

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await pet.remove({ session: sess });
    pet.creator.pets.pull(pet);
    await pet.creator.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not delete pet.",
      500
    );
    return next(error);
  }

  fs.unlink(imagePath, (err) => {
    console.log(err);
  });
  res.status(200).json({ message: "Deleted pet." });
};

exports.getPetById = getPetById;
exports.getPetsByUserId = getPetsByUserId;
exports.createPet = createPet;
exports.updatePet = updatePet;
exports.deletePet = deletePet;
exports.getPets = getPets;
exports.sendMail = sendMail;
